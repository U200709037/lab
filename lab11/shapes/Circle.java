package shapes;

public class Circle extends Shape {
	private double radius;

	
	public Circle(double radius) {
		super();
		this.radius = radius;
	}

	@Override
	public String toString() {
		return "Circle{" +
				"radius=" + radius +
				"} " + super.toString();
	}

	public double area(){
		return Math.PI * Math.pow(radius, 2);
	}
	
	
}
